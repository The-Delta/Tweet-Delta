<?php
/**
 * @file
 * User has successfully authenticated with Twitter. Access tokens saved to session and DB.
 */

/* Load required lib files. */
session_start();
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');


//Variables
$user = $_GET['u'];

/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: ./clearsessions.php');
}
/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

if (!empty($user)) { 
  $content = $connection->get('users/show', array('screen_name' => $user));
}
else {
  $content = $connection->get('account/verify_credentials');  
}
$profile_image_url = $content->{'profile_image_url'};
$profile_image_url = str_replace('normal', 'bigger', $profile_image_url);
?>
<!DOCTYPE html>
<html>
  <head>
    <title>The-Delta twitter client</title>
    <meta charset="utf-8"/>
    <style>
      * {
        margin: 0px;
        padding: 0px;
        font-size: 14px;
        font-family:Arial,Helvetica,sans-serif;
      }
      a {
        color: #FFFFFF;
        text-decoration: none;
      }
      .bio{
        background-image:url(<? print $content->{'profile_banner_url'}.'/web' ?>);
        color: white;
        width: 520px;
        height: 260px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        text-shadow: 0px 1px 1px rgba(17, 17, 17, 0.8);
      }
      .profile_image {
        margin: 20px;
        border: 5px solid white;
        border-radius: 5px;
        background-color: white;
        width: 73px;
        height: 73px;
        display: inline-table;
        color: blue;
      }
      #profile, .bio {
        margin-left: auto;
        margin-right: auto;
        text-align: center;
      }
      .location_url {
        position: relative;
        top: 20px;
      }
      .description {
        margin: 5px;
      }
      strong {
        display: block;
      }
      #stats {
        background-color: grey;
        border: 1px solid grey;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
        width: 518px;
        margin-left: auto;
        margin-right: auto;
      }
      .stats {
        display: inline-block;
        padding: 5px;
        border: 0.5px solid grey;
        background-color: lightgrey;
      }
      .a_stats {
        color: black;
        text-decoration: none;
      }
      #profile{
        display: table;
        box-shadow: 0px 0px 10px;
        border-radius: 5px;
      }
      h1 {font-size:220%;}
      h2 {font-size:200%;}
      h3 {font-size:180%;}
      h4 {font-size:160%;}
      h5 {font-size:140%;}
      h6 {font-size:120%;}
    </style>
  </head>
  <body>
    <?
      $created_at = str_replace("+0000", " ", $content->{'created_at'});
      print '<!--';
      print_r($content);
      print '-->';
      print '<div id=profile>';
        print '<div class="bio">';
          print '<img class="profile_image" src="'.$profile_image_url.'" alt="'.$content->{'screen_name'}.'"/>';
          print '<h4>'.$content->{'name'}.' - <a href="twitter.com/'.$content->{'name'}.'">@'.$content->{'screen_name'}.'</a></h4>';
          print '<div class="description">'.$content->{'description'}.'</div>';
          print '<div class="location_url">'.$content->{'location'}.'<span style="width:30px;"> </span><a href="'.$content->{'url'}.'">'.$content->{'url'}.'</a></div>';
        print '</div>';
        print '<div class="under_bio">';
          print '<div id="stats">';
            print '<span class="stats"><a class="a_stats" href="//twitter.com/'.$content->{'screen_name'}.'"><strong>Tweets:</strong>'.$content->{'statuses_count'}.'</a></span>';
            print '<span class="stats"><a class="a_stats" href="//twitter.com/'.$content->{'screen_name'}.'/following"><strong>Following:</strong>'.$content->{'friends_count'}.'</a></span>';
            print '<span class="stats"><a class="a_stats" href="//twitter.com/'.$content->{'screen_name'}.'/followers"><strong>Followers:</strong>'.$content->{'followers_count'}.'</a></span>';
            print '<span class="stats"><a class="a_stats" href="//twitter.com/'.$content->{'screen_name'}.'/memberships"><strong>Listed:</strong>'.$content->{'listed_count'}.'</a></span>';
            print '<span class="stats" style="width: 167px;"><strong>Created at:</strong>'.$created_at.'</span>';
            print '<span class="edit"></span>';
          print '</div>';
        print '</div>';
      print '</div>';
    ?>
  </body>
</html>
