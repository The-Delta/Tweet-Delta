<?php
session_start();

require_once('twitteroauth/twitteroauth.php');
require_once('config.php');

/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: ./clearsessions.php');
}

$_SESSION['expire'] = time() + (30 * 60);

/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

function tweet($arg_0, $arg_1) {
  $tweet = $arg_0;
  $nbr = $arg_1;

  for ($tw = 0;  $tw <= $nbr; $tw++) {
    print '<div class="tweet">';
      print '<div title="'.$tweet[$tw]->{'user'}->{'description'}.'" class="tweet_header">';
        print '<img class="tweet_avatar" src="'.$tweet[$tw]->{'user'}->{'profile_image_url'}.'" alt="@'.$tweet[$tw]->{'user'}->{'screen_name'}.' - '.$tweet[$tw]->{'user'}->{'name'}.'"/>';
        print '<span class="tweet_header_user">'.$tweet[$tw]->{'user'}->{'name'}.' - <a target="_blank" href="user?u='.$tweet[$tw]->{'user'}->{'screen_name'}.'">@'.$tweet[$tw]->{'user'}->{'screen_name'}.'</a></span>';
      print '</div>';
      print '<div class="tweet_body">'.$tweet[$tw]->{'text'}.'</div>';
      print '<div class="tweet_info">';
        print $tweet[$tw]->{'source'};
      print '</div>';
      print '<div class="tweet_action">';
        print '<a target="_blank" href="tweet?f='.$tweet[$tw]->{'id'}.'">fav</a> ';
        print '<a target="_blank" href="tweet?rt='.$tweet[$tw]->{'id'}.'">RT</a> ';
        print 'tweet_id: '.$tweet[$tw]->{'id'};
      print '</div>';
    print '</div>';
  }
}

function direct_message($arg_0, $arg_1) {
  $tweet = $arg_0;
  $nbr = $arg_1;

  for ($dm = 0;  $dm <= $nbr; $dm++) {
    print '<div class="tweet">';
      print '<div class="tweet_header">';
        print '<img class="tweet_avatar" src="'.$tweet[$dm]->{'sender'}->{'profile_image_url'}.'" alt="@'.$tweet[$dm]->{'sender'}->{'screen_name'}.' - '.$tweet[$dm]->{'sender'}->{'name'}.'"/>';
        print '<span class="tweet_header_user">'.$tweet[$dm]->{'sender'}->{'name'}.' - <a href="twitter.com/'.$tweet[$dm]->{'sender'}->{'screen_name'}.'">@'.$tweet[$dm]->{'sender'}->{'screen_name'}.'</a>';
      print '</div>';
      print '<div class="tweet_body">'.$tweet[$dm]->{'text'}.'</div>';
      print '<div class="tweet_info">';
        print str_replace("+0000", " ", $tweet[$dm]->{'created_at'});
      print '</div>';
    print '</div>';
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>The-Delta twitter client</title>
    <meta charset="utf-8"/>
    <link type="text/css" rel="stylesheet" href="css/index.css"/> 
  </head>
  <body>
    <div id="navbar">
      <form method="get" action="tweet">
        <textarea name="t"></textarea>
        <input type="text" name="re" placeholder="put tweet_id for reply"></input>
        <input type="submit" value="Tweet"></input>
      </form>
    </div>
    <div id="timeline" class="column">
      <div class="column_header">Timeline</div>
      <div class="column_content">
        <?
        $tweet = $connection->get('statuses/home_timeline');
        print '<!--';
        print_r($tweet);
        print '-->';
        tweet($tweet, 19);
      ?>
      </div>
    </div>
    <div id="mentions" class="column">
      <div class="column_header">Mentions</div>
      <div class="column_content">
      <?
        $tweet = $connection->get('statuses/mentions_timeline');
        tweet($tweet, 19);
      ?>
      </div>
    </div>
    <div class="column">
      <div class="column_header">Direct Messages</div>
      <div class="column_content">
      <?
        $tweet = $connection->get('direct_messages');

        print '<!--';
        print_r($tweet);
        print '-->';

        direct_message($tweet, 19);
      ?>
      </div>
    </div>
  </body>
</html>
