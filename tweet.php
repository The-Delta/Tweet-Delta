<?
  session_start();

  require_once('twitteroauth/twitteroauth.php');
  require_once('config.php');


  /* If access tokens are not available redirect to connect page. */
  if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: ./clearsessions.php');
  }
  /* Get user access tokens out of the session. */
  $access_token = $_SESSION['access_token'];

  /* Create a TwitterOauth object with consumer/user tokens. */
  $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

  $text = $_GET['t'];
  $repl = $_GET['re'];
  $fav  = $_GET['f'];
  $rt   = $_GET['rt'];

  if (!empty($text)) {
    if (!empty($repl)) {
      $result = $connection->post('statuses/update', array('status' => $text, 'in_reply_to_status_id' => $repl));
    } else {
      $result = $connection->post('statuses/update', array('status' => $text));
    }
  } elseif(!empty($fav)) {
    $result = $connection->post('favorites/create', array('id' => $fav));
  } elseif(!empty($rt)) {
    $result = $connection->post('statuses/retweet/'.$rt);
  } else {
    $result = 'Put text in the box OR GET THE HELL OUT OF HERE !';
  }

  print_r($result);
  print $result['errors']->{'0'}->{'message'};
?>
