<!DOCTYPE html>
<html>
  <head>
    <title>Connect to twitter</title>
    <meta charset="utf-8"/>
    <style>
      a {text-decoration: none;}
      body {
        background-color: black;
        text-align: center;
      }
      h1 {text-shadow: 2px 2px 2px black;}
      input {
        border-radius: 3px;
        border: none;
        padding: 3px;
      }
      input[type=submit] {background-image: linear-gradient(to bottom, #EDED11, #C4C422);}
      input[type=text], input[type=password], input[type=submit]:hover {
        box-shadow: 0px 0px 2px inset;
      }
      #box {
        margin-right: auto;
        margin-left: auto;
        margin-top: 20%;
        background-color: #333333;
        color: #A3A3A3;
        border-radius: 8px;
        box-shadow: 0px 0px 10px 5px #333333;
        display: table;
        padding: 8px;
      }
      #sign_in {
        border-radius: 3px;
        background-image: linear-gradient(to bottom, cyan, darkcyan);
        background-color: blue;
        color: black;
        padding: 5px;
        margin: 5px;
        display: inline-table;
        text-shadow: 0px 1px 0px white;
        font-weight: bold;
      }
      #sign_in:hover {text-shadow: 0px 0px 0px white;}
      #sign_in:active {
        background-image: linear-gradient(to top, cyan, darkcyan);
        background-color: darkblue;
      }
      .warn {color: red;}
    </style>
  </head>
  <body>
    <span id="box">
      <h1>Connect to TweetDelta</h1>
      <a href="./redirect.php"><span id="sign_in">Sign in with Twitter</span></a>
      <div class="warn">Login with The-Delta isn't implemented yet</div>
      <form action="login" method="post">
        <input name="user" type="text" placeholder="username"></input>
        <input name="passwd" type="password" placeholder="password"></input>
        <input type="submit">
      </form>
    </span>
  </body>
</html>
